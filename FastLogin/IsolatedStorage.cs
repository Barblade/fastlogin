﻿using System.IO;
using System.IO.IsolatedStorage;
using System.Web.Script.Serialization;

namespace FastLogin
{
    internal static class IsolatedStorage
    {
        public static void Save(object obj, string fileName)
        {
            IsolatedStorageFile isoStore = IsolatedStorageFile.GetStore(IsolatedStorageScope.User | IsolatedStorageScope.Assembly, null, null);

            using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream(fileName, FileMode.Create, isoStore))
            {
                using (StreamWriter writer = new StreamWriter(isoStream))
                {
                    writer.Write((new JavaScriptSerializer()).Serialize(obj));
                }
            }
        }
        
        public static object Load(string fileName)
        {
            IsolatedStorageFile isoStore = IsolatedStorageFile.GetStore(IsolatedStorageScope.User | IsolatedStorageScope.Assembly, null, null);

            if (isoStore.FileExists(fileName))
            {
                using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream(fileName, FileMode.Open, isoStore))
                {
                    using (StreamReader reader = new StreamReader(isoStream))
                    {
                        return new JavaScriptSerializer().Deserialize<Preferences>(reader.ReadToEnd());
                    }
                }
            }
            else
            {
                return null;
            }
        }
    }
}
