﻿namespace FastLogin
{
    using System;
    using System.Threading;
    using System.Windows.Forms;
    using System.Reflection;
    using System.IO;
    using System.Drawing;
    using System.Resources;
    using System.Globalization;
    using System.Collections;
    using System.Linq;
    using System.Text;

    public static class Program
    {
        private static System.Threading.Mutex singleton = new Mutex(true, "FastLogin");

        [STAThread]
        public static void Main()
        {
            if (!singleton.WaitOne(TimeSpan.Zero, true))
            {
                MessageBox.Show("FastLogin is already running !", "FastLogin", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FastLogin());
        }
    }
}
