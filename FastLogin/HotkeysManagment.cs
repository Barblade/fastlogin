﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace FastLogin
{
    internal class HotkeysManagement
    {
        private GlobalHotkey[] gHotkey = new GlobalHotkey[12];
        private GlobalHotkey smartExit;
        private GlobalHotkey preferences;
        private static IntPtr tibiaWindow;
        private Account[] accounts;
        private Form preferencesForm;
        private Form mainForm;

        public HotkeysManagement(Form form)
        {
            mainForm = form;
        }

        public void setPreferencesForm(Form preferencesForm)
        {
            this.preferencesForm = preferencesForm;
        }

        internal void HandleHotkey(int id)
        {
            int activeWin = ActiveWindow.GetStatus();
            long caretPosition;
            uint pid;

            if ((activeWin == 1) && (id >= 0) && (id < 12))
            {
                caretPosition = ActiveWindow.getCaretPositition();
                tibiaWindow = WinAPI.FindWindow(ActiveWindow.getClassName(), null);
                while (Control.ModifierKeys != Keys.None)
                    Thread.Sleep(10);

                Thread.Sleep(20);
                foreach (char c in accounts[id].AccountName)
                {
                    WinAPI.PostMessage(tibiaWindow, Constants.WM_CHAR, (IntPtr)c, IntPtr.Zero);
                }
                WinAPI.PostMessage(tibiaWindow, Constants.WM_KEYDOWN, (IntPtr)Constants.TAB, IntPtr.Zero);

                for (int i = 0; i < 20; i++)
                {
                    if (ActiveWindow.getClassName() == "TibiaClient" && caretPosition == ActiveWindow.getCaretPositition())
                        Thread.Sleep(10);
                }

                Thread.Sleep(20);

                foreach (char c in accounts[id].Password)
                {
                    WinAPI.PostMessage(tibiaWindow, Constants.WM_CHAR, (IntPtr)c, IntPtr.Zero);
                }
                WinAPI.PostMessage(tibiaWindow, Constants.WM_KEYDOWN, (IntPtr)Constants.ENTER, IntPtr.Zero);
            }
            
            else if ((activeWin == 2) && (id == Constants.SMARTEXIT_ID))
            {
                if (Constants.closeAllConnections == true)
                {
                    //TCPConnection.CloseRemotePort(7171);
                }
                else
                {
                    tibiaWindow = WinAPI.FindWindow(ActiveWindow.getClassName(), null);
                    WinAPI.GetWindowThreadProcessId(tibiaWindow, out pid);
                    TCPConnection.CloseProcessConnection(pid);
                }
            }
            else if (id == Constants.PREFERENCES_ID)
            {
                if (preferencesForm.Visible)
                {
                    preferencesForm.Hide();

                }
                else
                {
                    preferencesForm.Show();
                }
            }
        }

        internal void registerHotkeys(Hotkey loginHotkey, Hotkey exitHotkey, Account[] accounts)
        {
            this.accounts = accounts;
            int modifierValue = loginHotkey.getModifierValue();

            for (int i = 0; i < accounts.Length; i++)
            {
                if (accounts[i].AccountName != "" && accounts[i].Password != "")
                {
                    gHotkey[i] = new GlobalHotkey(modifierValue, accounts[i].Key, i, mainForm);
                    gHotkey[i].Register();
                }
            }
            smartExit = new GlobalHotkey(exitHotkey.getModifierValue(), exitHotkey.Key, Constants.SMARTEXIT_ID, mainForm);
            smartExit.Register();
            
        }

        internal void registerPreferenesHotkey()
        {
            preferences = new GlobalHotkey(Constants.CTRL + Constants.SHIFT + Constants.WIN, Constants.preferencesKey, Constants.PREFERENCES_ID, mainForm);
            preferences.Register();
        }

        internal void unregisterHotkeys()
        {
            for (int i = 0; i < 12; i++)
            {
                if (gHotkey[i] != null)
                {
                    gHotkey[i].Unregiser();
                }
            }
            smartExit.Unregiser();
            preferences.Unregiser();
        }
    }
}
