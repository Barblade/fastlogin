﻿namespace FastLogin
{
    using System;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;

    internal class GlobalHotkey
    {
        private int modifier;
        private int key;
        private IntPtr hWnd;
        private int id;

        public GlobalHotkey(int modifier, Keys key, int id, Form form)
        {
            this.modifier = modifier;
            this.key = (int)key;
            this.hWnd = form.Handle;
            this.id = id;
        }

        public override int GetHashCode()
        {
            return this.modifier ^ this.key ^ this.hWnd.ToInt32();
        }

        public bool Register()
        {
            return RegisterHotKey(this.hWnd, this.id, this.modifier, this.key);
        }

        public bool Unregiser()
        {
            return UnregisterHotKey(this.hWnd, this.id);
        }

        [DllImport("user32.dll")]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, int modifiers, int vk);
        [DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        private Keys GetKey(IntPtr lparam)
        {
            return (Keys)(lparam.ToInt32() >> 16);
        }
    }
}
