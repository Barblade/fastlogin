﻿using System.Windows.Forms;

namespace FastLogin
{
    public static class Constants
    {
        public const int loginWindow = 0x58F04C;  // 11 when login window, 0 when no login window
        public const int loginWindow2 = 0x7C52CC; // 9 when login window, 11 when char list
        public const int loginWindow11 = 0x4555C8; // (Qt5Core) 36/37 login window, opt general 141, opt status 124
        public const int loginWindow11alt = 0x482F88; //Qt5Gui+addr+0x0 -- 8 login window, 9 options/chars o
        public const int connInfo = 0x58F074; // 11 logged in, otherwise 0
        public const int caretPosition = 0x570740; // random values, changing when swhitching btwn login<>pass fields
        public const int connInfo11 = 0x69F840; // 8 logged in, 5 char list
        public const int WM_KEYDOWN = 0x0100;
        public const int WM_KEYUP = 0x0101;
        public const int WM_CHAR = 0x0102;
        public const int TAB = 0x09;
        public const int ENTER = 0x0D;
        public const int NOMOD = 0x0000;
        public const int ALT = 0x0001;
        public const int CTRL = 0x0002;
        public const int SHIFT = 0x0004;
        public const int WIN = 0x0008;
        public const int VK_SHIFT = 0x10;
        public const int VK_CONTROL = 0x11;
        public const int WM_HOTKEY_MSG_ID = 0x0312;
        public const int SMARTEXIT_ID = 13;
        public const int PREFERENCES_ID = 14;
        public const Keys preferencesKey = Keys.O;
        public const bool closeAllConnections = false;
        public static readonly long[] loginWindowOffsets = { 0x10, 0x4, 0xE0, 0x4, 0xC4, 0x1C };
        public static readonly long[] caretPositionOffsets = { 0x2C, 0x3C, 0x0 };
    }
}
