﻿namespace FastLogin
{
    partial class PreferencesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.PictureBox logo;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PreferencesForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.preferencesLabel = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.exitWin = new System.Windows.Forms.CheckBox();
            this.exitAlt = new System.Windows.Forms.CheckBox();
            this.exitShift = new System.Windows.Forms.CheckBox();
            this.exitCtrl = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.loginWin = new System.Windows.Forms.CheckBox();
            this.loginAlt = new System.Windows.Forms.CheckBox();
            this.loginShift = new System.Windows.Forms.CheckBox();
            this.loginCtrl = new System.Windows.Forms.CheckBox();
            this.exitKey = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.plusLabel = new System.Windows.Forms.Label();
            this.loginKey = new System.Windows.Forms.ComboBox();
            this.smartExitLabel = new System.Windows.Forms.Label();
            this.loginLabel = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.accountsGrid = new System.Windows.Forms.DataGridView();
            this.key = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accountName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.password = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.exitLabel = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.hideLabel = new System.Windows.Forms.Label();
            this.savePicture = new System.Windows.Forms.PictureBox();
            logo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(logo)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.accountsGrid)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.savePicture)).BeginInit();
            this.SuspendLayout();
            // 
            // logo
            // 
            logo.BackColor = System.Drawing.Color.Transparent;
            logo.Image = ((System.Drawing.Image)(resources.GetObject("logo.Image")));
            logo.InitialImage = ((System.Drawing.Image)(resources.GetObject("logo.InitialImage")));
            logo.Location = new System.Drawing.Point(12, 8);
            logo.Name = "logo";
            logo.Size = new System.Drawing.Size(50, 50);
            logo.TabIndex = 5;
            logo.TabStop = false;
            logo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.form_MouseDown);
            logo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.form_MouseMove);
            logo.MouseUp += new System.Windows.Forms.MouseEventHandler(this.form_MouseUp);
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.SlateGray;
            this.splitter1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(0, 65);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(469, 185);
            this.splitter1.TabIndex = 3;
            this.splitter1.TabStop = false;
            this.splitter1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.form_MouseDown);
            this.splitter1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.form_MouseMove);
            this.splitter1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.form_MouseUp);
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.Color.LightSlateGray;
            this.splitter2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter2.Location = new System.Drawing.Point(0, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(469, 65);
            this.splitter2.TabIndex = 4;
            this.splitter2.TabStop = false;
            this.splitter2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.form_MouseDown);
            this.splitter2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.form_MouseMove);
            this.splitter2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.form_MouseUp);
            // 
            // preferencesLabel
            // 
            this.preferencesLabel.AutoSize = true;
            this.preferencesLabel.BackColor = System.Drawing.Color.LightSlateGray;
            this.preferencesLabel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.preferencesLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.preferencesLabel.Location = new System.Drawing.Point(80, 18);
            this.preferencesLabel.Name = "preferencesLabel";
            this.preferencesLabel.Size = new System.Drawing.Size(227, 40);
            this.preferencesLabel.TabIndex = 6;
            this.preferencesLabel.Text = "Preferences";
            this.preferencesLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.form_MouseDown);
            this.preferencesLabel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.form_MouseMove);
            this.preferencesLabel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.form_MouseUp);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Font = new System.Drawing.Font("Arial Unicode MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tabControl1.Location = new System.Drawing.Point(11, 78);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(447, 160);
            this.tabControl1.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.exitWin);
            this.tabPage1.Controls.Add(this.exitAlt);
            this.tabPage1.Controls.Add(this.exitShift);
            this.tabPage1.Controls.Add(this.exitCtrl);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.loginWin);
            this.tabPage1.Controls.Add(this.loginAlt);
            this.tabPage1.Controls.Add(this.loginShift);
            this.tabPage1.Controls.Add(this.loginCtrl);
            this.tabPage1.Controls.Add(this.exitKey);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.plusLabel);
            this.tabPage1.Controls.Add(this.loginKey);
            this.tabPage1.Controls.Add(this.smartExitLabel);
            this.tabPage1.Controls.Add(this.loginLabel);
            this.tabPage1.Location = new System.Drawing.Point(4, 27);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(439, 129);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Hotkeys";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.form_MouseDown);
            this.tabPage1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.form_MouseMove);
            this.tabPage1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.form_MouseUp);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(347, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 18);
            this.label5.TabIndex = 30;
            this.label5.Text = "+";
            // 
            // exitWin
            // 
            this.exitWin.AutoSize = true;
            this.exitWin.Checked = true;
            this.exitWin.CheckState = System.Windows.Forms.CheckState.Checked;
            this.exitWin.Location = new System.Drawing.Point(295, 70);
            this.exitWin.Name = "exitWin";
            this.exitWin.Size = new System.Drawing.Size(49, 22);
            this.exitWin.TabIndex = 29;
            this.exitWin.Text = "Win";
            this.exitWin.UseVisualStyleBackColor = true;
            // 
            // exitAlt
            // 
            this.exitAlt.AutoSize = true;
            this.exitAlt.Location = new System.Drawing.Point(230, 70);
            this.exitAlt.Name = "exitAlt";
            this.exitAlt.Size = new System.Drawing.Size(43, 22);
            this.exitAlt.TabIndex = 28;
            this.exitAlt.Text = "Alt";
            this.exitAlt.UseVisualStyleBackColor = true;
            // 
            // exitShift
            // 
            this.exitShift.AutoSize = true;
            this.exitShift.Checked = true;
            this.exitShift.CheckState = System.Windows.Forms.CheckState.Checked;
            this.exitShift.Location = new System.Drawing.Point(155, 70);
            this.exitShift.Name = "exitShift";
            this.exitShift.Size = new System.Drawing.Size(54, 22);
            this.exitShift.TabIndex = 27;
            this.exitShift.Text = "Shift";
            this.exitShift.UseVisualStyleBackColor = true;
            // 
            // exitCtrl
            // 
            this.exitCtrl.AutoSize = true;
            this.exitCtrl.Checked = true;
            this.exitCtrl.CheckState = System.Windows.Forms.CheckState.Checked;
            this.exitCtrl.Location = new System.Drawing.Point(86, 70);
            this.exitCtrl.Name = "exitCtrl";
            this.exitCtrl.Size = new System.Drawing.Size(47, 22);
            this.exitCtrl.TabIndex = 26;
            this.exitCtrl.Text = "Ctrl";
            this.exitCtrl.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(274, 70);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 18);
            this.label6.TabIndex = 25;
            this.label6.Text = "+";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(134, 70);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 18);
            this.label7.TabIndex = 24;
            this.label7.Text = "+";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(209, 70);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 18);
            this.label8.TabIndex = 23;
            this.label8.Text = "+";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(347, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 18);
            this.label4.TabIndex = 22;
            this.label4.Text = "+";
            // 
            // loginWin
            // 
            this.loginWin.AutoSize = true;
            this.loginWin.Checked = true;
            this.loginWin.CheckState = System.Windows.Forms.CheckState.Checked;
            this.loginWin.Location = new System.Drawing.Point(295, 31);
            this.loginWin.Name = "loginWin";
            this.loginWin.Size = new System.Drawing.Size(49, 22);
            this.loginWin.TabIndex = 21;
            this.loginWin.Text = "Win";
            this.loginWin.UseVisualStyleBackColor = true;
            // 
            // loginAlt
            // 
            this.loginAlt.AutoSize = true;
            this.loginAlt.Location = new System.Drawing.Point(230, 31);
            this.loginAlt.Name = "loginAlt";
            this.loginAlt.Size = new System.Drawing.Size(43, 22);
            this.loginAlt.TabIndex = 20;
            this.loginAlt.Text = "Alt";
            this.loginAlt.UseVisualStyleBackColor = true;
            // 
            // loginShift
            // 
            this.loginShift.AutoSize = true;
            this.loginShift.Checked = true;
            this.loginShift.CheckState = System.Windows.Forms.CheckState.Checked;
            this.loginShift.Location = new System.Drawing.Point(155, 31);
            this.loginShift.Name = "loginShift";
            this.loginShift.Size = new System.Drawing.Size(54, 22);
            this.loginShift.TabIndex = 19;
            this.loginShift.Text = "Shift";
            this.loginShift.UseVisualStyleBackColor = true;
            // 
            // loginCtrl
            // 
            this.loginCtrl.AutoSize = true;
            this.loginCtrl.Location = new System.Drawing.Point(86, 31);
            this.loginCtrl.Name = "loginCtrl";
            this.loginCtrl.Size = new System.Drawing.Size(47, 22);
            this.loginCtrl.TabIndex = 18;
            this.loginCtrl.Text = "Ctrl";
            this.loginCtrl.UseVisualStyleBackColor = true;
            // 
            // exitKey
            // 
            this.exitKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.exitKey.FormattingEnabled = true;
            this.exitKey.IntegralHeight = false;
            this.exitKey.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"});
            this.exitKey.Location = new System.Drawing.Point(367, 69);
            this.exitKey.MaxDropDownItems = 3;
            this.exitKey.Name = "exitKey";
            this.exitKey.Size = new System.Drawing.Size(66, 26);
            this.exitKey.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(274, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 18);
            this.label2.TabIndex = 10;
            this.label2.Text = "+";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(134, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 18);
            this.label1.TabIndex = 9;
            this.label1.Text = "+";
            // 
            // plusLabel
            // 
            this.plusLabel.AutoSize = true;
            this.plusLabel.Location = new System.Drawing.Point(209, 31);
            this.plusLabel.Name = "plusLabel";
            this.plusLabel.Size = new System.Drawing.Size(16, 18);
            this.plusLabel.TabIndex = 8;
            this.plusLabel.Text = "+";
            // 
            // loginKey
            // 
            this.loginKey.BackColor = System.Drawing.SystemColors.Window;
            this.loginKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.loginKey.FormattingEnabled = true;
            this.loginKey.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.loginKey.Items.AddRange(new object[] {
            "F1,F2..",
            "1,2.."});
            this.loginKey.Location = new System.Drawing.Point(367, 29);
            this.loginKey.Name = "loginKey";
            this.loginKey.Size = new System.Drawing.Size(66, 26);
            this.loginKey.TabIndex = 5;
            this.loginKey.TextChanged += new System.EventHandler(this.loginKey_TextChanged);
            this.loginKey.Enter += new System.EventHandler(this.loginKey_Enter);
            // 
            // smartExitLabel
            // 
            this.smartExitLabel.AutoSize = true;
            this.smartExitLabel.Font = new System.Drawing.Font("Arial Unicode MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.smartExitLabel.Location = new System.Drawing.Point(6, 71);
            this.smartExitLabel.Name = "smartExitLabel";
            this.smartExitLabel.Size = new System.Drawing.Size(80, 18);
            this.smartExitLabel.TabIndex = 1;
            this.smartExitLabel.Text = "Smart Exit";
            // 
            // loginLabel
            // 
            this.loginLabel.AutoSize = true;
            this.loginLabel.Font = new System.Drawing.Font("Arial Unicode MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.loginLabel.Location = new System.Drawing.Point(6, 31);
            this.loginLabel.Name = "loginLabel";
            this.loginLabel.Size = new System.Drawing.Size(44, 18);
            this.loginLabel.TabIndex = 0;
            this.loginLabel.Text = "Login";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.accountsGrid);
            this.tabPage2.Location = new System.Drawing.Point(4, 27);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(439, 129);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Accounts";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // accountsGrid
            // 
            this.accountsGrid.AllowUserToAddRows = false;
            this.accountsGrid.AllowUserToDeleteRows = false;
            this.accountsGrid.AllowUserToResizeColumns = false;
            this.accountsGrid.AllowUserToResizeRows = false;
            this.accountsGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial Unicode MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.accountsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.accountsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.accountsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.key,
            this.accountName,
            this.password});
            this.accountsGrid.EnableHeadersVisualStyles = false;
            this.accountsGrid.Location = new System.Drawing.Point(6, 6);
            this.accountsGrid.Name = "accountsGrid";
            this.accountsGrid.RowHeadersVisible = false;
            this.accountsGrid.Size = new System.Drawing.Size(427, 117);
            this.accountsGrid.TabIndex = 0;
            this.accountsGrid.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.accountsGrid_CellFormatting);
            this.accountsGrid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.accountsGrid_CellValueChanged);
            this.accountsGrid.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.accountsGrid_EditingControlShowing);
            // 
            // key
            // 
            this.key.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.key.Frozen = true;
            this.key.HeaderText = "Key";
            this.key.MaxInputLength = 5;
            this.key.Name = "key";
            this.key.ReadOnly = true;
            this.key.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.key.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.key.Width = 37;
            // 
            // accountName
            // 
            this.accountName.Frozen = true;
            this.accountName.HeaderText = "Account Name";
            this.accountName.MaxInputLength = 100;
            this.accountName.Name = "accountName";
            this.accountName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.accountName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.accountName.Width = 180;
            // 
            // password
            // 
            this.password.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.password.HeaderText = "Password";
            this.password.MaxInputLength = 100;
            this.password.Name = "password";
            this.password.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.password.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Location = new System.Drawing.Point(4, 27);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(439, 129);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "About";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(294, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 18);
            this.label3.TabIndex = 0;
            this.label3.Text = "FastLogin v1.1 beta";
            // 
            // exitLabel
            // 
            this.exitLabel.AutoSize = true;
            this.exitLabel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.exitLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exitLabel.Font = new System.Drawing.Font("Arial Unicode MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.exitLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.exitLabel.Location = new System.Drawing.Point(441, 8);
            this.exitLabel.Name = "exitLabel";
            this.exitLabel.Padding = new System.Windows.Forms.Padding(1, 0, 0, 0);
            this.exitLabel.Size = new System.Drawing.Size(17, 16);
            this.exitLabel.TabIndex = 8;
            this.exitLabel.Text = "X";
            this.toolTip1.SetToolTip(this.exitLabel, "Exit");
            this.exitLabel.Click += new System.EventHandler(this.exitLabel_Click);
            this.exitLabel.MouseLeave += new System.EventHandler(this.label_MouseLeave);
            this.exitLabel.MouseHover += new System.EventHandler(this.exitLabel_MouseHover);
            // 
            // hideLabel
            // 
            this.hideLabel.AutoSize = true;
            this.hideLabel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.hideLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.hideLabel.Font = new System.Drawing.Font("Arial Unicode MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.hideLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.hideLabel.Location = new System.Drawing.Point(417, 8);
            this.hideLabel.Name = "hideLabel";
            this.hideLabel.Padding = new System.Windows.Forms.Padding(1, 0, 0, 0);
            this.hideLabel.Size = new System.Drawing.Size(15, 16);
            this.hideLabel.TabIndex = 9;
            this.hideLabel.Text = "_";
            this.toolTip1.SetToolTip(this.hideLabel, "Hide");
            this.hideLabel.Click += new System.EventHandler(this.hideLabel_Click);
            this.hideLabel.MouseLeave += new System.EventHandler(this.label_MouseLeave);
            this.hideLabel.MouseHover += new System.EventHandler(this.hideLabel_MouseHover);
            // 
            // savePicture
            // 
            this.savePicture.BackColor = System.Drawing.Color.LightSlateGray;
            this.savePicture.Image = ((System.Drawing.Image)(resources.GetObject("savePicture.Image")));
            this.savePicture.Location = new System.Drawing.Point(442, 42);
            this.savePicture.Name = "savePicture";
            this.savePicture.Size = new System.Drawing.Size(17, 17);
            this.savePicture.TabIndex = 10;
            this.savePicture.TabStop = false;
            this.toolTip1.SetToolTip(this.savePicture, "Save");
            this.savePicture.Click += new System.EventHandler(this.savePicture_Click);
            this.savePicture.MouseLeave += new System.EventHandler(this.savePicture_MouseLeave);
            this.savePicture.MouseHover += new System.EventHandler(this.savePicture_MouseHover);
            // 
            // PreferencesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 250);
            this.Controls.Add(this.savePicture);
            this.Controls.Add(this.hideLabel);
            this.Controls.Add(this.exitLabel);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.preferencesLabel);
            this.Controls.Add(logo);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.splitter1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PreferencesForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "FastLogin";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.PreferencesForm_Load);
            this.VisibleChanged += new System.EventHandler(this.PreferencesForm_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(logo)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.accountsGrid)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.savePicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Label preferencesLabel;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label smartExitLabel;
        private System.Windows.Forms.Label loginLabel;
        private System.Windows.Forms.ComboBox loginKey;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label plusLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox exitKey;
        private System.Windows.Forms.Label exitLabel;
        private System.Windows.Forms.CheckBox loginWin;
        private System.Windows.Forms.CheckBox loginAlt;
        private System.Windows.Forms.CheckBox loginShift;
        private System.Windows.Forms.CheckBox loginCtrl;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox exitWin;
        private System.Windows.Forms.CheckBox exitAlt;
        private System.Windows.Forms.CheckBox exitShift;
        private System.Windows.Forms.CheckBox exitCtrl;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView accountsGrid;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label hideLabel;
        private System.Windows.Forms.PictureBox savePicture;
        private System.Windows.Forms.DataGridViewTextBoxColumn key;
        private System.Windows.Forms.DataGridViewTextBoxColumn accountName;
        private System.Windows.Forms.DataGridViewTextBoxColumn password;
    }
}