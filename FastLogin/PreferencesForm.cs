﻿using System;
using System.Collections;
using System.Drawing;
using System.IO;
using System.Media;
using System.Net;
using System.Windows.Forms;

namespace FastLogin
{
    public partial class PreferencesForm : Form
    {
        ArrayList loginKeyModifiers = new ArrayList();
        ArrayList exitKeyModifiers = new ArrayList();
        HotkeysManagement hotkeyManagement;
        SoundPlayer soundPlayer;

        public PreferencesForm()
        {
            InitializeComponent();
        }

        internal void loadPreferences()
        {
            Preferences preferences;
            Hotkey loginHotkey;
            Hotkey exitHotkey;
            Account[] accounts;

            loginKey.SelectedIndex = 0;
            exitKey.Text = "L";
            accountsGrid.ClearSelection();
            accountsGrid.CurrentCell = null;

            preferences = (Preferences)IsolatedStorage.Load("preferences.json");

            if (preferences != null)
            {
                loginHotkey = preferences.LoginHotkey;
                exitHotkey = preferences.ExitHotkey;
                accounts = preferences.Accounts;

                loginCtrl.Checked = loginHotkey.Ctrl;
                loginShift.Checked = loginHotkey.Shift;
                loginAlt.Checked = loginHotkey.Alt;
                loginWin.Checked = loginHotkey.Win;

                exitCtrl.Checked = exitHotkey.Ctrl;
                exitShift.Checked = exitHotkey.Shift;
                exitAlt.Checked = exitHotkey.Alt;
                exitWin.Checked = exitHotkey.Win;
                exitKey.Text = exitHotkey.Key.ToString();

                if (preferences.MaxHotkeys == 10)
                {
                    loginKey.SelectedIndex = 1;
                }

                for (int i = 0; i < preferences.MaxHotkeys; i++)
                {
                    accountsGrid.Rows.Add(accounts[i].Key.ToString().Replace("D", ""), accounts[i].AccountName, accounts[i].Password);
                }

                hotkeyManagement.registerHotkeys(loginHotkey, exitHotkey, accounts);
            }
            else
            {
                accountsGrid.Rows.Add("F1", "barblade", "hackme123");
                for (int i = 2; i < 13; i++)
                {
                    accountsGrid.Rows.Add("F" + i, "", "");
                }
                Preferences pereferences = readFormPreferences();
                hotkeyManagement.registerHotkeys(pereferences.LoginHotkey, pereferences.ExitHotkey, pereferences.Accounts);
            }
        }

        internal void setHotkeyManagementForm(HotkeysManagement hotkeyManagement)
        {
            this.hotkeyManagement = hotkeyManagement;
        }

        private bool isMouseDown = false;
        private Point formPoint;
        private Point cursorPoint;

        private void form_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown)
            {
                Point difference = Point.Subtract(Cursor.Position, new Size(cursorPoint));
                Location = Point.Add(formPoint, new Size(difference));
            }
        }

        private void form_MouseDown(object sender, MouseEventArgs e)
        {
            isMouseDown = true;
            cursorPoint = Cursor.Position;
            formPoint = Location;
        }

        private void form_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
        }

        private void exitLabel_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to exit ?", "Confirmation", MessageBoxButtons.OKCancel);
            if (result == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        private void hideLabel_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void exitLabel_MouseHover(object sender, EventArgs e)
        {
            exitLabel.BackColor = Color.Crimson;
            exitLabel.ForeColor = Color.White;
        }

        private void hideLabel_MouseHover(object sender, EventArgs e)
        {
            hideLabel.BackColor = SystemColors.ControlDark;
            hideLabel.ForeColor = SystemColors.ControlText;
        }

        private void label_MouseLeave(object sender, EventArgs e)
        {
            (sender as Label).BackColor = SystemColors.ControlLight;
            (sender as Label).ForeColor = Color.Black;
            hideLabel.ForeColor = SystemColors.ControlDarkDark;
        }

        private void savePicture_MouseHover(object sender, EventArgs e)
        {
            savePicture.BackColor = Color.LightSkyBlue;
        }

        private void savePicture_MouseLeave(object sender, EventArgs e)
        {
            savePicture.BackColor = Color.LightSlateGray;
        }

        private Preferences readFormPreferences()
        {
            Keys key;
            int maxHotkeys;

            accountsGrid.ClearSelection();
            accountsGrid.CurrentCell = null;

            if (loginKey.SelectedIndex == 0)
            {
                maxHotkeys = 12;
            }
            else
            {
                maxHotkeys = 10;
            }

            Hotkey loginHotkey = new Hotkey(loginCtrl.Checked, loginShift.Checked, loginAlt.Checked, loginWin.Checked);
            Enum.TryParse(exitKey.Text, out key);
            Hotkey exitHotkey = new Hotkey(exitCtrl.Checked, exitShift.Checked, exitAlt.Checked, exitWin.Checked, key);

            Account[] accounts = new Account[maxHotkeys];
            string keyStr;

            for (int i = 0; i < accountsGrid.Rows.Count; i++)
            {
                keyStr = accountsGrid.Rows[i].Cells["key"].Value.ToString();
                if (keyStr[0] != 'F')
                {
                    keyStr = "D" + keyStr;
                }
                accounts[i] = new Account(
                    accountsGrid.Rows[i].Cells["accountName"].Value.ToString(),
                    accountsGrid.Rows[i].Cells["password"].Value.ToString(),
                    (Keys)Enum.Parse(typeof(Keys), keyStr)
                );
            }
            return new Preferences(loginHotkey, exitHotkey, maxHotkeys, accounts);
        }

        private void savePicture_Click(object sender, EventArgs e)
        {
            Preferences pereferences = readFormPreferences();
            IsolatedStorage.Save(pereferences, "preferences.json");
            hotkeyManagement.unregisterHotkeys();
            hotkeyManagement.registerHotkeys(pereferences.LoginHotkey, pereferences.ExitHotkey, pereferences.Accounts);
            hotkeyManagement.registerPreferenesHotkey();
        }

        bool loginKeyChaned = false;

        private void loginKey_TextChanged(object sender, EventArgs e)
        {
            if (!loginKeyChaned)
                return;

            string F = "";
            int count = 10;
            int k = 0;

            if (loginKey.Text.ToString()[0] == 'F')
            {
                F = "F";
                count = 13;
                k = 1;
                accountsGrid.Rows.Add("", "", "");
                accountsGrid.Rows.Add("", "", "");
            }
            else
            {
                accountsGrid.Rows.RemoveAt(11);
                accountsGrid.Rows.RemoveAt(10);
            }

            for (int i = 1; i < count; i++)
            {
                accountsGrid.Rows[i-1].Cells["key"].Value = F + i;
            }

            if (k == 0)
                accountsGrid.Rows[9].Cells["key"].Value = "0";
        }

        private void loginKey_Enter(object sender, EventArgs e)
        {
            loginKeyChaned = true;
        }

        private void accountsGrid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (accountsGrid.CurrentCell.Tag != null)
            {
                e.Control.Text = accountsGrid.CurrentCell.Tag.ToString();
            }
        }

        private void accountsGrid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (accountsGrid.Columns[e.ColumnIndex].Name != "key" && e.Value != null && e.Value.ToString() != "")
            {
                accountsGrid[e.ColumnIndex, e.RowIndex].Tag = e.Value;
                e.Value = new string('*', e.Value.ToString().Length);
            }
        }

        private void accountsGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >=0 && accountsGrid[e.ColumnIndex, e.RowIndex].Value == null)
            {
                accountsGrid[e.ColumnIndex, e.RowIndex].Value = "";
            }
        }


        private void PreferencesForm_VisibleChanged(object sender, EventArgs e)
        {
            if (soundPlayer == null)
            {
                return;
            }
            if (!Visible)
                soundPlayer.Stop();
            else
                soundPlayer.PlayLooping();
        }

        private void PreferencesForm_Load(object sender, EventArgs e)
        {
            WebClient webClient = new WebClient();
            webClient.DownloadDataCompleted += DownloadDataCompleted;
            webClient.DownloadDataAsync(new Uri("http://calmera.no-ip.org/fastlogin/theme.wav"));
        }

        private void DownloadDataCompleted(object sender,
            DownloadDataCompletedEventArgs e)
        {
            if  (e.Error == null)
            {
                byte[] theme = e.Result;
                Stream stream = new MemoryStream(theme);
                soundPlayer = new SoundPlayer(stream);
                soundPlayer.PlayLooping();
            }
        }
    }
}