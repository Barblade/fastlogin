﻿using System.Windows.Forms;

namespace FastLogin
{
    internal class Preferences
    {
        public Account[] Accounts { get; set; }
        public Hotkey ExitHotkey { get; set; }
        public Hotkey LoginHotkey { get; set; }
        public int MaxHotkeys { get; set; }

        public Preferences() { }
        public Preferences(Hotkey loginHotkey, Hotkey exitHotkey, int maxHotkeys, Account[] accounts)
        {
            LoginHotkey = loginHotkey;
            ExitHotkey = exitHotkey;
            MaxHotkeys = maxHotkeys;
            Accounts = new Account[MaxHotkeys];
            Accounts = accounts;
        }
    }

    internal class Hotkey
    {
        public bool Ctrl { get; set; }
        public bool Shift { get; set; }
        public bool Alt { get; set; }
        public bool Win { get; set; }
        public Keys Key { get; set; }

        public Hotkey() { }
        public Hotkey(bool ctrl, bool shift, bool alt, bool win, Keys key = Keys.KeyCode)
        {
            Ctrl = ctrl;
            Shift = shift;
            Alt = alt;
            Win = win;
            Key = key;
        }

        public int getModifierValue()
        {
            int modifier = 0;

            if (Ctrl)
            { 
                modifier += Constants.CTRL;
            }
            if (Shift)
            {
                modifier += Constants.SHIFT;
            }
            if (Alt)
            {
                modifier += Constants.ALT;
            }
            if (Win)
            {
                modifier += Constants.WIN;
            }

            return modifier;
        }
    }

    internal class Account
    {
        public string AccountName { get; set; }
        public string Password { get; set; }
        public Keys Key { get; set; }

        public Account() { }
        public Account(string accountName, string password, Keys key)
        {
            AccountName = accountName;
            Password = password;
            Key = key;
        }
    }
}
