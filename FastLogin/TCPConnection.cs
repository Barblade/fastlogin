﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace FastLogin
{
    public static class TCPConnection
    {
        public enum TCP_TABLE_CLASS
        {
            TCP_TABLE_BASIC_LISTENER,
            TCP_TABLE_BASIC_CONNECTIONS,
            TCP_TABLE_BASIC_ALL,
            TCP_TABLE_OWNER_PID_LISTENER,
            TCP_TABLE_OWNER_PID_CONNECTIONS,
            TCP_TABLE_OWNER_PID_ALL,
            TCP_TABLE_OWNER_MODULE_LISTENER,
            TCP_TABLE_OWNER_MODULE_CONNECTIONS,
            TCP_TABLE_OWNER_MODULE_ALL
        }
        public struct MIB_TCPROW_OWNER_PID
        {
            public uint state;
            public uint localAddr;
            public int localPort;
            public uint remoteAddr;
            public int remotePort;
            public uint owningPid;
        }
        public struct MIB_TCPTABLE_OWNER_PID
        {
            public uint dwNumEntries;
            [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.Struct, SizeConst = 1)]
            public MIB_TCPROW_OWNER_PID[] table;
        }
        public enum State
        {
            All = 0,
            Closed = 1,
            Listen = 2,
            Syn_Sent = 3,
            Syn_Rcvd = 4,
            Established = 5,
            Fin_Wait1 = 6,
            Fin_Wait2 = 7,
            Close_Wait = 8,
            Closing = 9,
            Last_Ack = 10,
            Time_Wait = 11,
            Delete_TCB = 12
        }
        public static void CloseRemotePort(int port)
        {
            MIB_TCPROW_OWNER_PID[] rows = GetExtendedTcpTable();
            for (int i = 0; i < rows.Length; i++)
            {
                if (port == WinAPI.ntohs(rows[i].remotePort))
                {
                    rows[i].state = (int)State.Delete_TCB;
                    IntPtr ptr = GetPtrToNewObject(rows[i]);
                    WinAPI.SetTcpEntry(ptr);
                }
            }
        }
        public static void CloseProcessConnection(uint pid)
        {
            MIB_TCPROW_OWNER_PID[] rows = GetExtendedTcpTable();
            for (int i=0; i < rows.Length; i++)
            {
                if (rows[i].owningPid == pid)
                {
                    rows[i].state = (int)State.Delete_TCB;
                    IntPtr ptr = GetPtrToNewObject(rows[i]);
                    WinAPI.SetTcpEntry(ptr);
                }
            }
        }
        private static MIB_TCPROW_OWNER_PID[] GetExtendedTcpTable()
        {
            MIB_TCPROW_OWNER_PID[] tTable;
            int AF_INET = 2;
            int buffSize = 0;

            uint ret = WinAPI.GetExtendedTcpTable(IntPtr.Zero, ref buffSize, true, AF_INET, TCP_TABLE_CLASS.TCP_TABLE_OWNER_PID_ALL);
            IntPtr buffTable = Marshal.AllocHGlobal(buffSize);

            try
            {
                ret = WinAPI.GetExtendedTcpTable(buffTable, ref buffSize, true, AF_INET, TCP_TABLE_CLASS.TCP_TABLE_OWNER_PID_ALL);
                if (ret != 0)
                {
                    return null;
                }

                MIB_TCPTABLE_OWNER_PID tab = (MIB_TCPTABLE_OWNER_PID)Marshal.PtrToStructure(buffTable, typeof(MIB_TCPTABLE_OWNER_PID));
                IntPtr rowPtr = (IntPtr)((long)buffTable + Marshal.SizeOf(tab.dwNumEntries));
                tTable = new MIB_TCPROW_OWNER_PID[tab.dwNumEntries];

                for (int i = 0; i < tab.dwNumEntries; i++)
                {
                    MIB_TCPROW_OWNER_PID tcpRow = (MIB_TCPROW_OWNER_PID)Marshal.PtrToStructure(rowPtr, typeof(MIB_TCPROW_OWNER_PID));
                    tTable[i] = tcpRow;
                    rowPtr = (IntPtr)((long)rowPtr + Marshal.SizeOf(tcpRow));
                }
            }
            finally
            {
                Marshal.FreeHGlobal(buffTable);
            }
            return tTable;
        }
        private static IntPtr GetPtrToNewObject(object obj)
        {
            IntPtr ptr = Marshal.AllocCoTaskMem(Marshal.SizeOf(obj));
            Marshal.StructureToPtr(obj, ptr, false);
            return ptr;
        }
    }
}
