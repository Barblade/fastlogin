﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Collections;

namespace FastLogin
{
    public static class Tibia
    {
        public static byte[] ReadBytes(IntPtr Handle, long Address, uint BytesToRead)
        {
            IntPtr ptrBytesRead;
            byte[] buffer = new byte[BytesToRead];
            WinAPI.ReadProcessMemory(Handle, new IntPtr(Address), buffer, BytesToRead, out ptrBytesRead);
            return buffer;
        }
        public static int ReadInt(long Address, IntPtr Handle)
        {
            return BitConverter.ToInt32(ReadBytes(Handle, Address, 4), 0);
        }
        public static string ReadString(long Address, IntPtr Handle, uint lenght = 32)
        {
            return Encoding.Default.GetString(ReadBytes(Handle, Address, lenght)).Split('\0')[0];
        }
        public static Process[] GetTibiaProcesses()
        {
            ArrayList tibiaProcesses = new ArrayList();
            if (Process.GetProcessesByName("Tibia").Length != 0)
                tibiaProcesses.AddRange(Process.GetProcessesByName("Tibia"));
            if (Process.GetProcessesByName("client").Length != 0)
                tibiaProcesses.AddRange(Process.GetProcessesByName("client"));
            if (tibiaProcesses.Count > 0)
                return tibiaProcesses.Cast<Process>().ToArray();
            else
                return null;
        }
        public static uint GetBaseAddress(Process tibiaProcess)
        {
            return (uint)tibiaProcess.MainModule.BaseAddress.ToInt32();
        }
        public static Process GetActiveProcess()
        {
            uint pid;
            WinAPI.GetWindowThreadProcessId(WinAPI.GetForegroundWindow(), out pid);
            return Process.GetProcessById((int)pid);
        }
        public static long GetAddressFromPointer(long address, IntPtr Handle, long[] offsets)
        {
            for (int i = 0; i < offsets.Count(); i++)
                address = ReadInt(address + offsets[i], Handle);
            return address;
        }
    }

    public static class ActiveWindow
    {
        private static Process[] tibiaProcesses;
        private static Process activeProcess;
        private static uint baseAddress;
        private static IntPtr tibiaHandle;
        private static Dictionary<string, IntPtr> modules  = new Dictionary<string, IntPtr>();
        private static bool isActive;
        private static StringBuilder className = new StringBuilder(256);

        public static int GetStatus()
        {
            tibiaProcesses = Tibia.GetTibiaProcesses();
            activeProcess = Tibia.GetActiveProcess();

            if (tibiaProcesses == null)
            {
                return 0;
            }

            isActive = false;

            foreach (Process p in tibiaProcesses)
            {
                if (p.Id == activeProcess.Id)
                {
                    isActive = true;
                }
            }

            if (!isActive)
            {
                return 0;
            }

            baseAddress = Tibia.GetBaseAddress(activeProcess);
            tibiaHandle = activeProcess.Handle;
            WinAPI.GetClassName(activeProcess.MainWindowHandle, className, className.Capacity);

            if (activeProcess.Modules.Count > 80)
            {
                int contInnfo = Tibia.ReadInt(Constants.connInfo11 + baseAddress, tibiaHandle);
                if (contInnfo == 8)
                {
                    return 2;
                }
                else if (contInnfo == 5)
                {
                    modules.Clear();
                    foreach (ProcessModule processModule in activeProcess.Modules)
                    {
                        modules.Add(processModule.ModuleName, processModule.BaseAddress);
                    }

                    long loginWinPtr = Tibia.ReadInt(Constants.loginWindow11 + (int)modules["Qt5Core.dll"], tibiaHandle);
                    long loginWin = Tibia.GetAddressFromPointer(loginWinPtr, tibiaHandle, Constants.loginWindowOffsets);
                    if (loginWin == 36 || loginWin == 37)
                        return 1;
                }

            }
            else if (activeProcess.Modules.Count <= 80)
            {
                if (Tibia.ReadInt(Constants.connInfo + baseAddress, tibiaHandle) == 11)
                    return 2;
                else if (Tibia.ReadInt(Constants.loginWindow + baseAddress, tibiaHandle) == 11 && Tibia.ReadInt(Constants.loginWindow2 + baseAddress, tibiaHandle) == 9)
                    return 1;
            }
            return 0;
        }
        public static string getClassName()
        {
            return className.ToString();
        }
        public static long getCaretPositition()
        {
            int caretPtr = Tibia.ReadInt(Constants.caretPosition + baseAddress, tibiaHandle);
            return Tibia.GetAddressFromPointer(caretPtr, tibiaHandle, Constants.caretPositionOffsets);
        }
    }

    public partial class FastLogin : Form
    {
        HotkeysManagement hotkeyManagement;
        PreferencesForm preferencesForm;

        public FastLogin()
        {
            InitializeComponent();
            preferencesForm = new PreferencesForm();
            hotkeyManagement = new HotkeysManagement(this);
            preferencesForm.setHotkeyManagementForm(hotkeyManagement);
            hotkeyManagement.setPreferencesForm(preferencesForm);
            preferencesForm.loadPreferences();
            hotkeyManagement.registerPreferenesHotkey();
        }

        protected override void SetVisibleCore(bool value)
        {
            if (!IsHandleCreated) CreateHandle();
            base.SetVisibleCore(false);
        }

        protected override void WndProc(ref Message m)
        { 
            if (m.Msg == Constants.WM_HOTKEY_MSG_ID)
            {
                hotkeyManagement.HandleHotkey(m.WParam.ToInt32());
            }
            base.WndProc(ref m);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            hotkeyManagement.unregisterHotkeys();
        }

        void Form1_Load(object sender, EventArgs e)
        {
            Size = new Size(0, 0);
        }
    }
}